//
// Created by Kai Kuhlmann on 04.05.16.
//

#ifndef OOA_PR3_DIGRAPH_H
#define OOA_PR3_DIGRAPH_H


#include <string>
#include "Node.h"
#include "Edge.h"

using namespace std;

class DiGraph {

private:
    vector<Node*> nodes;

public:
    DiGraph();
    ~DiGraph();

    void addNode(Node *node);
    void addEdge(string key1, string key2, float weight);

    vector<Node*> getNeighbours(string key);
    vector<Edge*> getEdges(string key);
    vector<Node*> getNodes();
};

DiGraph::DiGraph() {}
DiGraph::~DiGraph() {}

void DiGraph::addNode(Node *node) {
    nodes.push_back(node);
}

void DiGraph::addEdge(string key1, string key2, float weight) {
    Edge *edge = new Edge();

    for (int i = 0; i < nodes.size(); i++) {
        if (nodes[i]->getKey() == key1) {
            edge->setStartNode(nodes[i]);

            for (int j = 0; j < nodes.size(); j++) {
                if (nodes[j]->getKey() == key2) {
                    edge->setEndNode(nodes[j]);
                    edge->setWeight(weight);

                    nodes[i]->setNewEdge(edge);
                    break;
                }
            }
        }
    }
}

vector<Node*> DiGraph::getNeighbours(string key) {
    vector<Node*> nbs;
    vector<Edge*> node_edges = getEdges(key);

    for (int i = 0; i < node_edges.size(); i++) {
        nbs.push_back(node_edges[i]->getEndNode());
    }

    return nbs;
}

vector<Edge*> DiGraph::getEdges(string key) {
    vector<Edge*> edges;

    for (int i = 0; i < nodes.size(); i++) {
        if (nodes[i]->getKey() == key) {
            edges = nodes[i]->getEdges();
        }
    }

    return edges;
}

vector<Node*> DiGraph::getNodes() {
    return nodes;
}


#endif //OOA_PR3_DIGRAPH_H
