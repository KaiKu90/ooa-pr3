#include <iostream>
#include "DiGraph.h"
#include "OpenCVGraphVisualizer.h"

//
// Created by Kai Kuhlmann on 04.05.16.
//

using namespace std;

int main() {
    DiGraph *graph = new DiGraph();

    Node *node1 = new Node();
    node1->setKey("A");
    node1->setPositionX(200);
    node1->setPositionY(250);

    Node *node2 = new Node();
    node2->setKey("B");
    node2->setPositionX(250);
    node2->setPositionY(400);

    Node *node3 = new Node();
    node3->setKey("C");
    node3->setPositionX(450);
    node3->setPositionY(450);

    Node *node4 = new Node();
    node4->setKey("D");
    node4->setPositionX(650);
    node4->setPositionY(400);

    Node *node5 = new Node();
    node5->setKey("E");
    node5->setPositionX(550);
    node5->setPositionY(200);

    Node *node6 = new Node();
    node6->setKey("F");
    node6->setPositionX(400);
    node6->setPositionY(350);

    graph->addNode(node1);
    graph->addNode(node2);
    graph->addNode(node3);
    graph->addNode(node4);
    graph->addNode(node5);
    graph->addNode(node6);

    graph->addEdge("A", "B", 5);
    graph->addEdge("A", "E", 10);
    graph->addEdge("B", "C", 7);
    graph->addEdge("B", "F", 1);
    graph->addEdge("C", "D", 9);
    graph->addEdge("D", "E", 3);
    graph->addEdge("E", "F", 3);
    graph->addEdge("F", "D", 2);
    graph->addEdge("F", "A", 6);

    vector<Node*> nbs = graph->getNeighbours("A");
    vector<Edge*> edg = graph->getEdges("A");

    OpenCVGraphVisualizer *visualizer = new OpenCVGraphVisualizer();
    visualizer->render(*graph);
    visualizer->show();

    return 0;
}